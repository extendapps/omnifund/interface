/**
 * @file OmniFund.js
 * @copyright 2018 Extend Apps
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
export declare class OmniFund {
    private static instance;
    private constructor();
    static getInstance(domain: string): OmniFund;
}

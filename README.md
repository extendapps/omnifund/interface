# OmniFund - Interface

> Interface to OmniFund's SuiteApp

[![npm version](https://badge.fury.io/js/%40extendapps%2Fomnifund.svg)](https://badge.fury.io/js/%40extendapps%2Fomnifund)

## Install

```bash
npm install --save-dev @extendapps/omnifund
```

## Usage

> TODO

## License

[MIT](http://vjpr.mit-license.org)